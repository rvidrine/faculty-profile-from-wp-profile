<?php
	/*
		Template Name: Faculty Profile from WP Profile
	*/
?>
<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>
        <div id="mainContent">
			<div class="wfCollegeOne">
                <?php if (have_posts()) : ?>
			
             			 
                        <?php while (have_posts()) : the_post(); ?>
                        
                            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                            <?php 
								$deacnet_name = get_post_meta( $post->ID, 'deacnet_name', 'true'); // get their LDAP username as entered in custom field
								$user_profile_data = get_user_by( 'email', $deacnet_name . '@wfu.edu' ); // add username to domain, then get user data by email
								$first_name = $user_profile_data->first_name;
								$last_name = $user_profile_data->last_name;
								$email = $user_profile_data->user_email;
								$website = $user_profile_data->user_url;
								$phone = $user_profile_data->wfco_ophone; // get value of field added in functions.php
								$office = $user_profile_data->wfco_olocation; // another field added
								$research = $user_profile_data->wfco_research_interests; // many people won't populate this field, display below is conditional
							?>

                                <div class="entry">
									<div id="profArea">
										<div id="profName"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></div>
										<div id="profPicArea">
											<div id="profPicIndent">
												<?php echo get_wp_user_avatar( $email ); ?>
											</div><!-- Close of profPicIndent-->
										</div><!-- Close of profPicArea-->
										<div class="profContact">
											<div id="office"><span class="label">Office:</span> <span class="profilefield officlocation"><?php echo esc_html( $office ); ?></span></div>
											<div id="email"><span class="label">Email:<span> <span class="profilefield email"><?php echo '<a href="' . esc_attr( $email ) . '" >' . esc_html( $email ) . '</a>'; ?></span></div>
											<div id="phone"><span class="label">Phone:</span> <span class="profilefield phone"><?php echo esc_html( $phone ); ?></span></div>
											<div id="website"><?php if( $website ) {
												echo '<span class="label">Website:</span> <span class="profilefield website">' . '<a href="' . esc_url( $website ) . '">' . $website . '</a></span>'; }; ?></div>
											<div id="research"><?php if( $research ) {
												echo '<span class="label">Research Interests:</span> <span class="profilefield research">' . esc_html( $research ) . '</span>'; }; ?>
											</div>
										</div>
									</div>
								
									<?php the_content(); ?>
									</div><!--End of entry div -->
                                </div><!-- Close of post div-->
                                
                        <?php endwhile; ?>
                        
                <?php else : ?>
                <?php endif; ?>
                </div><!-- Close of wfCollegeOne div-->
            </div><!-- Close of mainContent-->
<?php get_footer(); ?>
