Faculty Profile Fields
Version 0.9

How to use this:
1. Put facultyprofilefields.php into a folder called inc in your theme folder (it's already in WF College One Pro)
2. Copy/paste the profile fields part of the facultyprofile.php page template into whatever page template you want to use. (Again, this is already installed in WF College One Pro, though this has the one addition of get_wp_user_avatar in the profile pic div, which relies on also installing WP User Avatar. If you want to just use regular avatars from Gravatar or whatever, you don't need that plugin; just change that line to get_avatar and it will use the default WP avatar behavior.)
3. When creating a profile page, select "Faculty Profile from WP user profile" template.
4. Scroll down to Custom Fields (a normal WP feature) on the page editor, and (the first time), click Enter New and type in deacnet_name for the name of the field. (After you enter the field once on your site, you can just choose it from the drop-down.)
5. Put the faculty or staff member's WFU username in the Value column next to deacnet_name.

You have several CSS selectors to style up the output of the field labels:
#profArea - div wrapping the whole set of fields
#profName - div containing the first and last names
#profPicArea - div wrapper for profile pic
#profPicIndent - div container for photo itself
.profContact - div container for all the contact information type fields
#office - office location
#email - um... their email address
#phone - office phone
#website - website address CONDITIONAL, only displays if populated
#research - research interests CONDITIONAL

If you have other fields you want to add, or want to make additional fields conditional, follow the syntax and add your own stuff. You might notice that almost all of these are IDs, meaning that the CSS rule has to start with #, and also is used to indicate something that only appears ONCE on the page. If you want to put a bunch of people
s profile info on the same page, you'll have to do some more programming to differentiate between different sets of user data, and also change these to classes instead of IDs.